class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy #as Post is master and comments are child, hence applying cascade delete

	#validation : start
	validates_presence_of :title
	validates_presence_of :body
	##validation : end
	
end
